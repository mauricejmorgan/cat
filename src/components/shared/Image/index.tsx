import React, { FC } from  'react';
import styles from './image.module.css';

interface ImageProps {
	src: string
}

const Image: FC<ImageProps>  = ({
  src
}) => {
  return (<img className={styles.img} alt='' src={src} />);
}

export default Image;