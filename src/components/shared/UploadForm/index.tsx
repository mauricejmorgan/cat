import React, { useState, FC } from 'react';
import styles from './upload-form.module.css';
import Button from '../Button';

interface UploadFormProps {
	onFileUpload: (e: FormData) => void,
  text?: string
}

const UploadForm: FC<UploadFormProps> = ({
  onFileUpload,
  text = 'Upload'
}) => {
  const [file, setFile] = useState<File>();
  
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (files) setFile(files[0]);
  }

  const onClick = () => {
    const formData = new FormData();
    if (file) {
      formData.append("file", file, file?.name);
      onFileUpload(formData);
    }
  }

  return <div className={styles.container}>
    <input type="file" onChange={onChange} className={styles.fileInput}/>
    <div>
    <Button onClick={onClick} text={text} />
    </div>
  </div>;
};

export default UploadForm;