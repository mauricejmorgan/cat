import React, { useState, FC, Children, isValidElement, cloneElement } from 'react';

import Button from '../Button';

export type ToggleValue = 'ON' | 'OFF';

interface ToggleButtonProps {
  toggleText?: string[]
  icon?: string,
  value?: ToggleValue
  onToggle?: (toggled: ToggleValue) => void
}

const iconStyle = [{'fill': 'white'}, {'fill': 'red'}];

const ToggleButton: FC<ToggleButtonProps> = ({
  toggleText = [],
  children,
  value = 'OFF',
  onToggle
}) => {
  const [toggle, setToggle] = useState<ToggleValue>(value);

  const onClick = () => {
    if (onToggle) onToggle(toggle);

    setToggle(toggle === 'OFF' ? 'ON' : 'OFF');
  }

  const childrenWithProps = Children.map(children, child => 
    (isValidElement(child)) ? cloneElement(child, { 'style': iconStyle[(toggle === 'OFF') ? 0 : 1] }) : child);

  return <Button text={toggleText[(toggle === 'OFF') ? 0 : 1]} onClick={onClick}>
    {childrenWithProps}
  </Button>
};

export default ToggleButton;