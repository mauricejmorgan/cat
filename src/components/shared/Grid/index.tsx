import React, { FC } from  'react';
import styles from './grid.module.css';

interface GridProps {
	container?: boolean;
  size?: 'i1' | 'i3' | 'i4';
  item?: boolean;
}

const Grid: FC<GridProps>  = ({
  container,
  item,
  size = 'i1',
  children
}) => <>
  { container && <div className={`${styles.container}`}>{children}</div> }
  { item && <div className={`${styles.item} ${styles[size]}`}>{children}</div> }
</>

export default Grid;