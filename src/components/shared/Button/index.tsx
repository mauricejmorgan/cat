import React, { FC } from 'react';
import styles from './button.module.css';

interface ButtonProps {
  text?: string,
	onClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
}

const Button: FC<ButtonProps> = ({
  text,
  children,
  onClick
}) => <button className={styles.button} onClick={onClick}>
  { text && text }
  { children && children }
</button> 

export default Button;