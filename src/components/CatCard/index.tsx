import React, { FC } from 'react';
import ToggleButton, { ToggleValue } from '../shared/ToggleButton';
import Image from '../shared/Image';

import styles from './cat-card.module.css';
import { FaHeart, FaArrowUp, FaArrowDown } from 'react-icons/fa';

interface CatCardProps {
  id: string;
  favId?: string;
  voteUp?: number;
  voteDown?: number;
	imageUrl: string;
  onFavToggle?: (id: string, favId?: string) => void;
  onVoting?: (id: string, vote: number) => void;
}

const CatCard: FC<CatCardProps>  = ({
  id,
  imageUrl,
  favId,
  voteUp = 0,
  voteDown = 0,
  onFavToggle,
  onVoting
}) => {

  const onToggle = (toggle: ToggleValue) => onFavToggle && onFavToggle(id, favId)

  const onVote = (vote: number) => () => onVoting && onVoting(id, vote);

  return (
    <div className="wrapper">
    <div className={styles.card}>
      <div className={styles.content}>
        <Image src={imageUrl} />
      </div>
      <div className={styles.footer}>
        <div className={styles.fav_buttons}>
          <ToggleButton onToggle={onToggle} value={ favId ? 'OFF' : 'ON' }>
            <FaHeart size={18}/>
          </ToggleButton>
        </div>
        <div className={styles.vote_buttons}>
          <FaArrowUp onClick={onVote(1)}/>
          <FaArrowDown onClick={onVote(0)}/>
        </div>
        <p className={styles.vote_info}>Up Votes: {voteUp}, Down Votes: {voteDown}</p>
      </div>
    </div>
  </div>
  );
}
export default CatCard;
