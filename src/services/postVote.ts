import axios from 'axios';
import ImageDetails from '../types/ImageDetails';
import imageDetailsMapper from '../mappers/imageDetailsMapper'

export const postVote = async (image_id:string, value: number): Promise<ImageDetails> => {
  const imageDetails = (await axios({
    url: `${process.env.REACT_APP_CAT_API}${process.env.REACT_APP_POST_VOTES}`,
    method: 'post',
    headers: {
      'x-api-key': process.env.REACT_APP_CAT_API_KEY
    },
    data: { image_id, value }
  })).data;

  return imageDetailsMapper(imageDetails)
}

export default postVote;