import axios from 'axios';

export const removeFavourite = async (favId:string): Promise<void> => {
  await axios({
    url: `${process.env.REACT_APP_CAT_API}${process.env.REACT_APP_POST_FAVOURITES}${favId}`,
    method: 'delete',
    headers: {
      'x-api-key': process.env.REACT_APP_CAT_API_KEY
    }
  });
}

export default removeFavourite;