import axios from 'axios';
import ImageDetails from '../types/ImageDetails';
import imageDetailsMapper from '../mappers/imageDetailsMapper'

export const getImages = async (): Promise<ImageDetails[]> => {
  const imageDetails = (await axios({
    url: `${process.env.REACT_APP_CAT_API}${process.env.REACT_APP_GET_IMAGES}?limit=100&sub_id=${process.env.REACT_APP_SUB_ID}`,
    method: 'get',
    headers: {
      'x-api-key': process.env.REACT_APP_CAT_API_KEY
    },
  })).data;
  return imageDetails.map((imageDetail: any) => imageDetailsMapper(imageDetail))
}

export default getImages;