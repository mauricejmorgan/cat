import axios from 'axios';
import { ImageFav } from '../types/ImageDetails';

export const addFavourite = async (image_id: string): Promise<ImageFav> => {
  const favResponse = (await axios({
    url: `${process.env.REACT_APP_CAT_API}${process.env.REACT_APP_POST_FAVOURITES}`,
    method: 'post',
    headers: {
      'x-api-key': process.env.REACT_APP_CAT_API_KEY
    },
    data: { image_id, sub_id: process.env.REACT_APP_SUB_ID },
  })).data;
  return { id: image_id, favId : favResponse.id };
}

export default addFavourite;