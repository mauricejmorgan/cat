import axios from 'axios';
import { ImageVote } from '../types/ImageDetails';
import Vote from '../types/Vote';

export const getImagesVotes = async (): Promise<ImageVote> => {
  
  const votes = (await axios({
    url: `${process.env.REACT_APP_CAT_API}${process.env.REACT_APP_GET_VOTES}?limit=100`,
    method: 'get',
    headers: {
      'x-api-key': process.env.REACT_APP_CAT_API_KEY
    }
  })).data as Vote[];

  const voteCount = votes.reduce<ImageVote>((p, n) => {
    
    const { value, image_id } = n;
    
    const addVote = (p[image_id]?.id === image_id) ? p : { [image_id]: { id: image_id, voteUp: 0, voteDown: 0 } }


    const field = (value === 1) ? 'voteUp' : 'voteDown';

    return { ...p, ...{[image_id] : {
        id: image_id,
        voteUp: (field === 'voteUp') ? Number(addVote[image_id].voteUp) + 1 : Number(addVote[image_id].voteUp),
        voteDown: (field === 'voteDown') ? Number(addVote[image_id].voteDown) + 1 : Number(addVote[image_id].voteDown),
    }}}
 
  }, {} as ImageVote);

  return voteCount;
}

export default getImagesVotes;