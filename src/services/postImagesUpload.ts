import axios from 'axios';
import ImageDetails from '../types/ImageDetails';
import imageDetailsMapper from '../mappers/imageDetailsMapper'

export const postImagesUpload = async (formData: FormData): Promise<ImageDetails> => {
  if (process.env.REACT_APP_SUB_ID) {
    formData.append('sub_id', process.env.REACT_APP_SUB_ID);
  }
  const imageDetails = (await axios({
    url: `${process.env.REACT_APP_CAT_API}${process.env.REACT_APP_POST_IMAGES_UPLOAD}`,
    method: 'post',
    headers: {
      'content-type': 'multipart/form-data;',
      'x-api-key': process.env.REACT_APP_CAT_API_KEY
    },
    data: formData
  })).data;
  return imageDetailsMapper(imageDetails)
}

export default postImagesUpload;