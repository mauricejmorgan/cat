import { useReducer, Dispatch, useMemo } from 'react';
import { AsyncDispatch, ThunkAction } from '../reducers/storeReducer';

function wrapAsync<T>(dispatch: Dispatch<T>): AsyncDispatch<T> {
  return function(action: T | ThunkAction<T>) {
    if (action instanceof Function) {
      return action(dispatch)
    }
    return dispatch(action)
  }
}

const useAsyncReducer = (
  reducer: Parameters<typeof useReducer>[0],  
  defaultState: Parameters<typeof useReducer>[1],
  initializer?: Parameters<typeof useReducer>[2]
): [ReturnType<typeof useReducer>[0], ReturnType<typeof wrapAsync>] => {
  const [state, dispatch] = useReducer(reducer, defaultState, initializer);
  const asyncDispatch = useMemo(() => wrapAsync(dispatch), [dispatch]);
  return [state, asyncDispatch];
}

 export default useAsyncReducer;
