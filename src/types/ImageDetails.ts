interface ImageDetails {
  id: string;
  favId?: string;
  vote?:number;
  voteUp?: number;
  voteDown?: number;
  height: number;
  original_filename: string;
  url: string;
  width: number;
}

export type ImageFav = Pick<ImageDetails, "favId" | "id">;
export type ImageVoteItem = Pick<ImageDetails, "id"> & { 
  voteUp?: number,
  voteDown?: number
};

export interface ImageVote { 
  [key: string]: ImageVoteItem;
};

export default ImageDetails;