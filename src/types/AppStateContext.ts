import { createContext } from 'react';
import { AsyncDispatch } from '../reducers/storeReducer';
import ImageDetails, { ImageFav, ImageVote } from './ImageDetails';

export interface State {
  imageDetails: ImageDetails[],
  errorMessages?: Record<'upload', string>
}

export interface AppStateContext<T, A> {
  state: [T, AsyncDispatch<A>]
}

export const appStateContext = createContext<AppStateContext<State, Action>>({} as AppStateContext<State, Action>);

export const AppStateProvider = appStateContext.Provider;
export const AppStateConsumer = appStateContext.Consumer;

export type Action =
  | { type: 'ADD_IMAGE_DETAILS_LIST'; data: ImageDetails[] }
  | { type: 'ADD_IMAGE_DETAILS'; data: ImageDetails }
  | { type: 'ADD_FAV'; data: ImageFav }
  | { type: 'REMOVE_FAV'; data: ImageFav }
  | { type: 'SET_ERROR_MESSAGE'; data: string }
  | { type: 'UPDATE_VOTES'; data: ImageVote }
  | { type: 'RESET_ERROR_MESSAGE' }
