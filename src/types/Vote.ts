interface Vote {
  id: number
  country_code: string
  created_at: Date
  image_id: string
  sub_id: string
  value: number
}

export default Vote;