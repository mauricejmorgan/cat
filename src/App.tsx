import React, { useEffect } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { AppStateProvider } from './types/AppStateContext';
import { defaultState, reducer } from './reducers/storeReducer';
import { CatListView, CatUploadView } from './views';
import getImages from './thunks/getImages';

import './App.css';
import useAsyncReducer from './hooks/useAsyncReducer';

const App = () => {

  const [state, dispatch] = useAsyncReducer(reducer, defaultState);

  useEffect(() => dispatch(getImages()), [dispatch]);
  
  return (
    <>
      <header/>
      <main>
      <AppStateProvider value={{ state: [state, dispatch] }}>
        <section>
          <BrowserRouter>
            <Switch>
		          <Route path="/" exact component={ CatListView }/>
		          <Route path="/upload" component={ CatUploadView }/>
            </Switch>
          </BrowserRouter>
        </section>
        </AppStateProvider> 
      </main>
    </>
  );
}

export default App;


