import { Dispatch } from "react";
import { Action, State } from "../types/AppStateContext";

export type ThunkAction<T> = (dispatch: Dispatch<T>) => T | Promise<T | void>
export type AsyncDispatch<T> = Dispatch<T | ThunkAction<T>>

export const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'ADD_IMAGE_DETAILS_LIST':
      return {
        ...state,
        imageDetails: action.data
      }
    case 'ADD_IMAGE_DETAILS':
        return {
          ...state,
          imageDetails: state.imageDetails.concat(action.data)
        }
    case 'SET_ERROR_MESSAGE':
      return {
        ...state,
        errorMessages: { 'upload': action.data }
      };
    case 'RESET_ERROR_MESSAGE':
      return {
        ...state,
        errorMessages: undefined
      };
    case 'ADD_FAV':
    case 'REMOVE_FAV':
      const { id, ...favourite } = action.data;
      return {
        ...state,
        imageDetails: state.imageDetails.map(imageDetail => imageDetail.id === id ? { ...imageDetail, ...favourite } : imageDetail)
      };
    case 'UPDATE_VOTES':
        const votes = action.data;
        return {
          ...state,
          imageDetails: state.imageDetails.map(imageDetail => {
              const foundVote = votes[imageDetail.id];
              return (foundVote) ? { ...imageDetail, ...{ voteUp: foundVote.voteUp, voteDown: foundVote.voteDown } } : imageDetail
            })
        };
  }
}

export const defaultState = {
  imageDetails: []
};