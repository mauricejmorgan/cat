import { Dispatch } from "react";
import { ThunkAction } from "../reducers/storeReducer";
import postImagesUpload from "../services/postImagesUpload";
import { Action } from "../types/AppStateContext";

const ERROR_UPLOAD_IMAGES = 'An error occured when trying to upload image!';

const uploadImage = (uploadData: FormData): ThunkAction<Action> =>
  async (dispatch: Dispatch<Action>): Promise<Action | void> => {
    try {
      const imageDetails = await postImagesUpload(uploadData);
      dispatch({ type: "ADD_IMAGE_DETAILS", data: imageDetails });
      return { type: "ADD_IMAGE_DETAILS", data: imageDetails };
    } catch (ex) {
      dispatch({ type: "SET_ERROR_MESSAGE", data: ERROR_UPLOAD_IMAGES })
    }
  }

export default uploadImage;