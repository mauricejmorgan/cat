import { Dispatch } from "react";
import { ThunkAction } from "../reducers/storeReducer";
import removeFavourite from "../services/removeFavourite";
import { Action } from "../types/AppStateContext";

const removeFavImage = (id: string, favId: string): ThunkAction<Action> =>
  async (dispatch: Dispatch<Action>): Promise<void> => {
    await removeFavourite(favId);
    dispatch({ type: "REMOVE_FAV", data: {id, favId: undefined} });
  }

export default removeFavImage;