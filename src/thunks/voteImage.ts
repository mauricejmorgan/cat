import { Dispatch } from "react";
import { ThunkAction } from "../reducers/storeReducer";
import postVote from "../services/postVote";
import getVotes from "../services/getVotes";
import { Action } from "../types/AppStateContext";

const voteImage = (id: string, vote: number): ThunkAction<Action> =>
  async (dispatch: Dispatch<Action>): Promise<void> => {
    await postVote(id, vote);
    const votes = await getVotes();
    dispatch({ type: "UPDATE_VOTES", data: votes })
  
  }

export default voteImage;