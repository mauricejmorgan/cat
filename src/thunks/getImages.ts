import { Dispatch } from "react";
import { ThunkAction } from "../reducers/storeReducer";
import { getImages as getImagesService } from "../services/getImages";
import getVotes from "../services/getVotes";
import { Action } from "../types/AppStateContext";

const uploadImages = (): ThunkAction<Action> =>
  async (dispatch: Dispatch<Action>): Promise<void> => {
    const data = await getImagesService();
    dispatch({ type: "ADD_IMAGE_DETAILS_LIST", data })
    const votes = await getVotes();
    dispatch({ type: "UPDATE_VOTES", data: votes })
  }

export default uploadImages;