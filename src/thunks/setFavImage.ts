import { Dispatch } from "react";
import { ThunkAction } from "../reducers/storeReducer";
import addFavourite from "../services/addFavourite";
import { Action } from "../types/AppStateContext";

const setFavImage = (id: string): ThunkAction<Action> =>
  async (dispatch: Dispatch<Action>): Promise<void> => {
    const imageFav = await addFavourite(id);
    dispatch({ type: "ADD_FAV", data: imageFav });
  }

export default setFavImage;