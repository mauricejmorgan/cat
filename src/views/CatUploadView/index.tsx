import React, { useContext, useEffect } from 'react';
import UploadForm from '../../components/shared/UploadForm';
import { Action, appStateContext } from '../../types/AppStateContext';
import uploadImage from '../../thunks/uploadImage';
import { useHistory } from 'react-router-dom';

const CatUploadView = () => {

  const appStore = useContext(appStateContext);
  const [state, dispatch] = appStore.state;
  const history = useHistory();

  useEffect(() => dispatch({ 'type': 'RESET_ERROR_MESSAGE' }), [dispatch]);

  const onFileUpload = async (uploadData: FormData) => {
    const action = (await dispatch(uploadImage(uploadData))) as unknown as Action;
    if (action.type === 'ADD_IMAGE_DETAILS') history.push(`/`);
  }

  return <>
    {
      state.errorMessages &&
      state.errorMessages.upload &&
      <div>{state.errorMessages.upload}</div>
    }
    <UploadForm onFileUpload={onFileUpload} />
  </>
};

export default CatUploadView;