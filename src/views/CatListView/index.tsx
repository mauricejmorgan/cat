import React, { useContext } from 'react';
import { appStateContext } from '../../types/AppStateContext';
import { CatCard, Grid } from '../../components';
import setFavImage from '../../thunks/setFavImage';
import voteImage from '../../thunks/voteImage';
import removeFavImage from '../../thunks/removeFavImage';
import styles from './cat-list-view.module.css';

function CatListView() {
  const appStore = useContext(appStateContext);
  const [state, dispatch] = appStore.state;

  const onFavToggle = (id: string, favId?: string) => (favId) ? 
    dispatch(removeFavImage(id, favId)) : 
    dispatch(setFavImage(id))

  const onVoting = (id: string, vote: number) => dispatch(voteImage(id,vote))
  
  return <>
  <Grid container>
    {state?.imageDetails.length === 0 && 
      <p className={styles.noResults}>No Cats</p>
    }
    {state?.imageDetails.map((imageDetails, i) => 
      <Grid item size='i4'  key={i.toString()}>
        <CatCard 
          imageUrl={imageDetails.url} 
          id={imageDetails.id}
          favId={imageDetails.favId}
          voteUp={imageDetails.voteUp}
          voteDown={imageDetails.voteDown}
          onFavToggle={onFavToggle}
          onVoting={onVoting}/>
      </Grid>
    )}
  </Grid>
  </>;
};

export default CatListView;