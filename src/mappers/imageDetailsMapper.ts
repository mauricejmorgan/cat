import ImageDetails from '../types/ImageDetails';

export const imageDetailsMapper = (imageDetails: any): ImageDetails => {
  return {
    id: imageDetails.id,
    favId: imageDetails?.favourite?.id,
    vote: imageDetails?.vote?.value,
    height: imageDetails.height,
    original_filename: imageDetails.original_filename,
    url: imageDetails.url,
    width: imageDetails.width,
  };
}

export default imageDetailsMapper;